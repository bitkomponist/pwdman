const {password:readPassword,question:readQuestion} = require('./io');
const {isPristine,changePassword:changeStoragePassword,validateCredentials,write,get:getItem,set:setItem,init:initStorage} = require('./storage');
const beautify = require('./beautify');
const parseArgs = require('./args');
const yaml = require('js-yaml');
const {readFileSync} = require('fs');
const {resolve} = require('path');
const {read:readClipboard,write:writeClipboard} = require('./clipboard');

module.exports = class CLI {
    constructor(argv){

        if(isPristine()){
            this.init();
            return;
        }

        const {p:password,f:format,argv:[command,...args]} = parseArgs(argv);

        Object.assign(this,{
            password,
            format,
            args,
            argv
        });

        if(typeof(this[`${command}Command`]) === 'function'){
            this[`${command}Command`]();
        }else{
            this.args = [command,...args];
            this.readCommand();
        }
    }

    async init(){
        console.log(`please set a password: `);
        const password = await readPassword();
        console.log(`please repeat the password: `);
        const repeat = await readPassword();
        if(password !== repeat){
            console.log(`no match, try again`);
            await this.init();
            return;
        }

        if(initStorage(password)){
            console.log('created keys file');
            process.exit(0);
        }else{
            console.log('failed to init');
            process.exit(-1);
        }
    }

    async getPassword(){
        let {password} = this;
        if(!password){
            console.log("password:");
            password = await readPassword();
        }

        if(!validateCredentials(password)){
            this.password = undefined;
            console.log("incorrect password, try again");
            return await this.getPassword();
        }

        return (this.password = password);
    }

    async getEntry(path){
        const passwordInput = await this.getPassword();
        const {format} = this;

        path || (path = await readQuestion("what do you need? "));

        if(path){
            const item = getItem(passwordInput,path);
            if(item){
                if(typeof(item) === 'number' || typeof(item) === 'string'){
                    writeClipboard(item);
                }
                process.stdout.write(beautify(item,format));
                process.exit(0);
            }else if(item === undefined){
                console.log(path+" is not set");
                process.exit(1);
            }
        }
    }

    async setEntry(path,value,parse){
        if(!path){
            path = (await readQuestion("path: ")).trim();
        }
        if(!value){
            value = (await readQuestion("value: "));
        }

        if(!path || !value){
            return await this.setEntry(path,value);
        }

        if(parse){
            if(parse === 'json'){
                value = JSON.parse(value);
            }
            if(parse === 'yaml'){
                value = yaml.safeLoad(value);
            }
        }

        return setItem(await this.getPassword(), path,value);
    }

    async unsetEntry(path){
        if(!path){
            path = (await readQuestion("path: ")).trim();
        }

        if(!path){
            return await this.unsetEntry(path);
        }

        return setItem(await this.getPassword(), path, undefined);
    }

    async changePasswordCommand(){
        const password = await this.getPassword();

        console.log(`please set a new password: `);
        const nextPassword = await readPassword();
        console.log(`please repeat the new password: `);
        const repeat = await readPassword();
        if(nextPassword !== repeat){
            console.log(`no match, try again`);
            await this.changePassword();
        }else if(nextPassword === password){
            console.log('nothing changed');
            process.exit(0);
        }else{
            changeStoragePassword(passwordInput,nextPassword);
            console.log("changed password");
        }
    }

    async helpCommand(){
        process.stdout.write(readFileSync(resolve(__dirname,'../help.txt'),{encoding:'utf8'}));
    }

    async versionCommand(){
        process.stdout.write(require(resolve(__dirname,'../package.json')).version);
    }

    async readCommand(){
        await this.getEntry(this.args[0]);
    }

    async getPipedContent(){
        let data = '';
        const {stdin} = process;

        return new Promise((res)=>{
            if(stdin.isTTY){
                res(undefined);
                return;
            }

            stdin.on('readable', function() {
                const chunk = stdin.read();
                if(chunk !== null){
                    data += chunk;
                }
            });

            stdin.on('end', function() {
                res(data+"");
            });
        });
    }

    async addCommand(){
        const {format,args} = this;
        if(format){
            let value = args.slice(1).join(' ');
            const piped = await this.getPipedContent();
            if(piped !== undefined) {
                value = piped;
            }
            await this.setEntry(args[0],value,format);
        }else if(args.length){
            const items = args;
            for(let i = 0; i<items.length;i+=2){
                await this.setEntry(args[i],args[i+1]);
            }
        }else{
            this.setEntry();
        }
    }

    async dropCommand(){
        await this.unsetEntry(this.args[0]);
    }
}