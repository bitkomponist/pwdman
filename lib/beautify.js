const formatters = {
    yaml(value){
        return require('js-yaml').dump(
            value,
            {
                flowLevel: 3,
                styles: {
                    '!!null' : 'camelcase'
                }
            }
        )
    },
    json(value){
        return JSON.stringify(value,null,'\t');
    }
};

module.exports = (value,format="yaml") => {
    if(typeof(value) === 'object'){
        if(!(format in formatters)){
            throw new Error("invalid format "+format);
        }

        return formatters[format](value);
    }else{
        return value;
    }
}