const commands = {
    darwin:{
        read:"pbpaste",
        write:"pbcopy"
    },
    linux:{
        read:null,
        write:null
    },
    win32:{
        read:null,
        write:null
    },
    openbsd:{
        read:null,
        write:null
    },
    sunos:{
        read:null,
        write:null
    },
    freebsd:{
        read:null,
        write:null
    },
    freebsd:{
        read:null,
        write:null
    }
};

const {execSync} = require('child_process');

exports.read = ()=>{
    const {read} = commands[process.platform];
    if(!read) throw new Error('clipboard is not supported on your platform');
    return execSync(read);
}

exports.write = (value)=>{
    const {write} = commands[process.platform];
    if(!write) throw new Error('clipboard is not supported on your platform');
    return execSync(write,{input:value});
}