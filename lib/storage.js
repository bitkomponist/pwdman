const {readFileSync,writeFileSync,existsSync,mkdirSync} = require('fs');
const {resolve} = require('path');
const {createCipheriv,createDecipheriv,createHash} = require('crypto');
const algorithm = 'aes-128-cbc';
const rootDir = resolve(require('os').homedir(),'.pwdman');
const masterFile = resolve(rootDir,'db');

const hash = (s)=>{
    const h = createHash('sha256');
    h.update(s);
    return h.digest('hex');
}

exports.isPristine = ()=>!existsSync(masterFile)
const ensureRootDir = ()=>{
    if(exports.isPristine() && !existsSync(rootDir)){
        mkdirSync(rootDir);
    }
}

exports.read = password => {
    ensureRootDir();
    if(!existsSync(masterFile)) return false;
    const ciphered = readFileSync(masterFile,{encoding:'hex'});
    const decipher = createDecipheriv(algorithm,hash(password).slice(0,16),hash("schnitzelfabrik").slice(0,16));
    let dec = decipher.update(ciphered,'hex','utf8');
    dec += decipher.final('utf8');

    try {
        return JSON.parse(dec);
    }catch(e){
        return false;
    }
}

exports.validateCredentials = password => {
    try {
        return exports.read(password) !== false;
    }catch(e){
        return false;
    }
}

exports.write = (password,data) => {
    ensureRootDir();
    data = JSON.stringify(data);
    const cipher = createCipheriv(algorithm,hash(password).slice(0,16),hash("schnitzelfabrik").slice(0,16));
    let c = cipher.update(data,'utf8','hex');
    c += cipher.final('hex');
    writeFileSync(masterFile,c,{encoding:'hex'});

    return true;
}

exports.init = (password) => {
    return exports.write(password,{crdate:new Date(), keys:{}});
}

exports.get = (password,path) => {
    const {keys:data} = exports.read(password);
    if(!data) return false;
    return path.split('.').reduce((p,c)=>(p?p[c]:null),data);
}

exports.set = (password,path,value) => {
    const {keys:data,...rest} = exports.read(password);
    if(!data) return false;
    const fragments = path.split('.');
    const key = fragments.pop();
    const target = fragments.reduce((p,c)=>{
        return p[c] || (p[c] = {});
    },data);
    target[key] = value;
    return exports.write(password,{...rest,keys:data});
}

exports.changePassword = (old,next) => {
    const data = exports.read(old);
    if(!data){
        return false;
    }

    return exports.write(next,data);
}