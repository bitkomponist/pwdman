module.exports = (args = process.argv.slice(2))=>{
    const result = {argv:[]};

    for(let i=0;i < args.length; ++i){
        if(args[i].charAt(0) === '-'){
            if(args[i+1]!==undefined){
                result[args[i].substr(1)] = args[i+1];
                i++;
            }else{
                result[args[i].substr(1)] = true;
            }
        }else{
            result.argv.push(args[i]);
        }
    }

    return result;
}