const {createInterface} = require('readline');
const {Writable} = require('stream');

exports.question = (question="",muted = false) => new Promise((res)=>{
    const mutableStdout = new Writable({
        write: function(chunk, encoding, callback) {
            if (!this.muted)
            process.stdout.write(chunk, encoding);
            callback();
        }
    });    
    const rl = createInterface({input:process.stdin, output:mutableStdout, terminal:true});
    mutableStdout.muted = muted;
    rl.question(question,(answer)=>{
        rl.close();
        res(answer);
    });
});

exports.password = (question) => exports.question(question,true);